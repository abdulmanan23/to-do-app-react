import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { Task } from "./../models/task";

interface SingleTaskProps {
  readonly colored: boolean;
}

interface Props {
  task: Task;
}

const SlingleTaskWrapper = styled.li<SingleTaskProps>`
  width: 100%;
  max-width: 480px;
  margin: 0 auto ;
  border: ${(props) =>
    props.colored ? "1px solid #5077F1" : "1px solid #8BF150"};
  border-radius: 5px;
  list-style-type: none;
  padding: 20px 0;
  margin-bottom: 10px;
  background-color: ${(props) => (props.colored ? "#5077F1 " : "#5077F1")};
  color: white;
  font-size: 18px;  
`;

export const SingleTask: FunctionComponent<Props> = ({ task }) =>   
(
  <SlingleTaskWrapper colored={task.id % 2 === 0}>
    {task.name}
  </SlingleTaskWrapper>
 
);
