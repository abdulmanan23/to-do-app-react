import React, { FC } from 'react';
import { SingleTask } from './SingleTask';


export const TaskList: FC<any> = ({ tasks}: any) => {
  return(
    <ul style={{padding: 0}}>
    {tasks.map((task : any) => (
      <SingleTask key={task.id} task={task} />
    ))}
  </ul>
  )
    };
