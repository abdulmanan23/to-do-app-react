import React from 'react';
import {GoogleLogin} from 'react-google-login';
import {useHistory} from "react-router-dom";
import "./login.css";
import {CLIENT_ID,TEXT,COOKIE_POLICY} from "../constants";

export default function Login() {

  const history=useHistory();


  const responseGoogle = (response:any) => {
    if(response.accessToken )
    {
      localStorage.setItem("token",response.accessToken)
      history.push("/app");
    }
  }

  return (
    <div className="login-container" >
      <h2>Click Button Below to login</h2>
        <GoogleLogin
          clientId={CLIENT_ID}
          buttonText={TEXT}
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy={COOKIE_POLICY}
          className="button"
  />     
    </div>
  )
}
 
